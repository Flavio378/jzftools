<?php
/**
 * Class for Setting adapter.
 *
 * @category   Model
 * @package    ${namespacemapper}
 * @copyright  Copyright (c) 2013 CookieShop.
 * @license    http://www.gnu.org/licenses/gpl-3.0-standalone.html GNU GPL v.3.0
 * @author     Eduardo Ortiz <eduardooa1980@gmail.com>
 */
class ${namespacemapper} extends Zend_Db_Table_Abstract{
    /**
     * Table name
     * @var type 
     */
    protected $_name = '${nametable}';
}
