<?php
/**
 * Class for Setting adapter.
 *
 * @category   Model
 * @package    ZendX_Dojo_Toolbartab_${namespacemapper}
 * @copyright  Copyright (c) 2013 CookieShop.
 * @license    http://www.gnu.org/licenses/gpl-3.0-standalone.html GNU GPL v.3.0
 * @author     Eduardo Ortiz <eduardooa1980@gmail.com>
 */
class ZendX_Dojo_Toolbartab_${namespacemapper}{
    /**
     * 
     * @param type $id
     * @return type
     */
    public function getMetadata($id){
        return array(
            array(
            'resource'=> '${resource}/write',   
            'label'=>'\"Nuevo\"',
            'iconClass' => '\"dijitEditorIcon dijitEditorIconNewPage\"',
            'onClick' => 'function(){dijit.byId(\'${iddialognew}\').show()}'     
            ),
            array(
            'resource'=> '${resource}/update',   
            'label'=>'\"Editar\"',
            'iconClass' => '\"dijitEditorIcon dijitEditorIconPaste\"',
            'onClick' => 'function(){sendUpdate(\"grid_'.$id.'\",\"${iddialogedit}\",\"${resource}/findid\",\"${nameform}\",layaoutEdit${namespacelayout})}'    
            ),     
            array(
            'resource'=> '${resource}/delete',   
            'label'=>'\"Eliminar\"',
            'iconClass' => '\"commonIcons dijitIconDelete\"',
            'onClick' => 'function(){sendDelete(\"divgrid_'.$id.'\",\"grid_'.$id.'\",\"${resource}/delete\",\"${resource}/index\",layout${namespacelayout})}'    
            ),            
            array(
            'resource'=> '${resource}/filtros',   
            'label'=>'\"Filtros\"',
            'iconClass' => '\"commonIcons dijitIconEditTask\"',
            'onClick' => 'function(){dijit.byId(\'${iddialogfiltro}\').show()}'    
            ),
            array(
            'resource'=> '${resource}/getpaginas',   
            'label'=>'\"Actualizar Grid\"',
            'iconClass' => '\"commonIcons dijitIconUndo\"',
            'onClick' => 'function(){reloadGrid(\"grid_'.$id.'\",\"divgrid_'.$id.'\",\"${resource}/index\",layout${namespacelayout})}'    
            ),            
            array(
            'resource'=> '${resource}/getpaginas',   
            'label'=>'\"\"',
            'iconClass' => '\"dojoxGridPaginatorGotoDiv dojoxGridfirstPageBtn\"',
            'onClick' => 'function(){firstPage(\"grid_'.$id.'\",\"divgrid_'.$id.'\",\"${resource}/index\",layout${namespacelayout},\"${resource}/getpaginas\")}'    
            ),
            array(
            'resource'=> '${resource}/getpaginas',   
            'label'=>'\"\"',
            'iconClass' => '\"dojoxGridPaginatorGotoDiv dojoxGridprevPageBtn\"',
            'onClick' => 'function(){prevPage(\"grid_'.$id.'\",\"divgrid_'.$id.'\",\"${resource}/index\",layout${namespacelayout},\"${resource}/getpaginas\")}'    
            ),
            array(
            'resource'=> '${resource}/getpaginas',   
            'label'=>'\"\"',
            'iconClass' => '\"dojoxGridPaginatorGotoDiv dojoxGridnextPageBtn\"',
            'onClick' => 'function(){nextPage(\"grid_'.$id.'\",\"divgrid_'.$id.'\",\"${resource}/index\",layout${namespacelayout},\"${resource}/getpaginas\")}'    
            ),
            array(
            'resource'=> '${resource}/getpaginas',   
            'label'=>'\"\"',
            'iconClass' => '\"dojoxGridPaginatorGotoDiv dojoxGridlastPageBtn\"',
            'onClick' => 'function(){lastPage(\"grid_'.$id.'\",\"divgrid_'.$id.'\",\"${resource}/index\",layout${namespacelayout},\"${resource}/getpaginas\")}'    
            ),                    
        );
    }     
}
