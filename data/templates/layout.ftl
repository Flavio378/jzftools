
/**
 * layout grid.
 *
 * @category   layout grid
 * @package    ${controller}
 * @copyright  Copyright (c) 2013 CookieShop.
 * @license    http://www.gnu.org/licenses/gpl-3.0-standalone.html GNU GPL v.3.0
 * @author     Eduardo Ortiz <eduardooa1980@gmail.com>
 */
var layout${layoutname}=[[
<#list entitiescamel.rows as row> 
        {field:'${row.getField()}',name:'${row.getName()}',width: '50px'},
</#list> 
]];    

var layaoutEdit${layoutname}=[[
<#list entitiescamel.rows as row> 
        {field:"${row.getController()}Edit-${row.getFieldedit()}"},
</#list> 
]]; 
