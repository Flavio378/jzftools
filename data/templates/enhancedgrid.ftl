<?php
/**
 * Class for Setting adapter.
 *
 * @category   Model
 * @package    ${namespacemapper}
 * @copyright  Copyright (c) 2013 CookieShop.
 * @license    http://www.gnu.org/licenses/gpl-3.0-standalone.html GNU GPL v.3.0
 * @author     Eduardo Ortiz <eduardooa1980@gmail.com>
 */
class ${namespacemapper}{
    /**
     * 
     * @return type
     */
    public function getMetadata(){
        return array(
<#list entitiescamel.rows as row> 
        array('field' => '${row.getEntities()}', 'name' => '${row.getNamecamel()}', 'width' => '50px'),
</#list> 
        );
    }    
}
