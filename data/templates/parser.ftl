<?php
/**
 * Class for Entities.
 *
 * @category   Model
 * @package    ${namespacemapper}
 * @copyright  Copyright (c) 2013 CookieShop.
 * @license    http://www.gnu.org/licenses/gpl-3.0-standalone.html GNU GPL v.3.0
 * @author     Eduardo Ortiz <eduardooa1980@gmail.com>
 */
class ${namespacemapper}{
<#list entitiescamel.rows as row>	
    /**
     *
     * @var ${row.getTypedat()} 
     */
    protected $_${row.getEntities()};
</#list> 

    /**
     * 
     * @param array $options
     */
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * 
     * @param type $name
     * @param type $value
     * @throws Exception
     */
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Users property');
        }
        $this->$method($value);
    }

    /**
     * 
     * @param type $name
     * @return type
     * @throws Exception
     */
    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Users property');
        }
        return $this->$method();
    }

    /**
     * 
     * @param array $options
     * @return \Settings_Model_Permissions
     */
    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                    $this->$method($value);
            }
        }
        return $this;
    }
<#list entitiescamel.rows as row>
    /**
     * 
     * @return ${row.getTypedat()}
     */
    public function get${row.getNamecamel()}(){
        return $this->_${row.getEntities()};
    }
    /**
     * 
     * @param ${row.getTypedat()} $${row.getEntities()}
     * @return \${namespacemapper}
     */
    public function set${row.getNamecamel()}($${row.getEntities()}){
        $this->_${row.getEntities()}=(${row.getTypedat()})$${row.getEntities()};
        return $this;
    }
</#list>    
}
