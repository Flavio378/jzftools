<?php
/**
 * Class for Controller.
 *
 * @category   Controller
 * @package    ${module}_${controller}Controller
 * @copyright  Copyright (c) 2013 CookieShop.
 * @license    http://www.gnu.org/licenses/gpl-3.0-standalone.html GNU GPL v.3.0
 * @author     Eduardo Ortiz <eduardooa1980@gmail.com>
 */
class ${module}_${controller}Controller extends Zend_Controller_Action{
    public function init()
    {
        /* Initialize action controller here */
       $this->_helper->viewRenderer->setNoRender ();
       $this->_helper->getHelper ( 'layout' )->disableLayout();
    }

    public function indexAction()
    {
        // action body
        $request = $this->getRequest()->getRawBody();
        Zend_Json::$useBuiltinEncoderDecoder = true;
        $setting = new ${module}_Model_${controller}Mapper();
        if ($request) {
            $json2array = $this->_helper->getHelper('Jsontoarray');
            $arreglo = $json2array->setJsontoarray($request);
            $entries = $setting->read($arreglo['irapagina']);
        } else {
            $entries = $setting->read(1);
        }   
        
        $data = new Zend_Dojo_Data();
        $data->setIdentifier('id');
        $data->addItems($entries);
        $this->getResponse()->clearAllHeaders();
        $this->getResponse()
                ->setHeader('Content-Type', 'application/json; charset=utf-8');
        $this->getResponse()->setBody($data->toJson());        

    }
    
    public function writeAction() {
        // action body
        $request = $this->getRequest()->getRawBody();
        Zend_Json::$useBuiltinEncoderDecoder = true;
        $json2array = $this->_helper->getHelper('Jsontoarray');
        $arreglo = $json2array->setJsontoarray($request);
        if ($this->getRequest()->isXmlHttpRequest()) {
            $entidades = new ${module}_Model_${controller}($arreglo);
            $mapper = new ${module}_Model_${controller}Mapper();
            $mapper->write($entidades);
        }
    }
    public function findidAction() {
        // action body
        $rows = array();
        $this->getResponse()
                ->setHeader('Content-Type', 'application/json; charset=utf-8');
        $request = $this->getRequest()->getRawBody();
        Zend_Json::$useBuiltinEncoderDecoder = true;
        $json2array = $this->_helper->getHelper('Jsontoarray');
        $arreglo = $json2array->setJsontoarray($request);
        if ($this->getRequest()->isXmlHttpRequest()) {
            $tipear = new ${module}_Model_${controller}($arreglo);
            $mapper = new ${module}_Model_${controller}Mapper();
            $rest = $mapper->findid($tipear, array('${nameform}Edit'));            
            $jsonData = Zend_Json::encode(
                            array('identifier' => '${nameform}Edit-id',
                                'items' => $rest));
            $this->getResponse()->setBody($jsonData);
        }
    }
    
    public function deleteAction(){
        $this->getResponse()
                ->setHeader('Content-Type', 'application/json; charset=utf-8');
        $request = $this->getRequest()->getRawBody();
        Zend_Json::$useBuiltinEncoderDecoder = true;
        $json2array = $this->_helper->getHelper('Jsontoarray');
        $arreglo = $json2array->setJsontoarray($request);
        if ($this->getRequest()->isXmlHttpRequest()) {
            $tipear = new ${module}_Model_${controller}($arreglo);
            $mapper = new ${module}_Model_${controller}Mapper();
            $mapper->deleteRoles($tipear);            
        }        
    }
    
    public function filtrosAction(){
        $request = $this->getRequest()->getRawBody();
        Zend_Json::$useBuiltinEncoderDecoder = true;
        $json2array = $this->_helper->getHelper('Jsontoarray');
        $arreglo = $json2array->setJsontoarray($request);
        if ($this->getRequest()->isXmlHttpRequest()) {      
            $tipear = new ${module}_Model_${controller}($arreglo);
            $mapper = new ${module}_Model_${controller}Mapper();
            $entries = $mapper->filtros($tipear);            
            $data = new Zend_Dojo_Data();
            $data->setIdentifier('id');
            $data->addItems($entries);
            $this->getResponse()->clearAllHeaders();
            $this->getResponse()
                    ->setHeader('Content-Type', 'application/json; charset=utf-8');
            $this->getResponse()->setBody($data->toJson());  
        }        
    }

    public function getpaginasAction(){
        $this->getResponse()
                ->setHeader('Content-Type', 'application/json; charset=utf-8');
        $mapper = new ${module}_Model_${controller}Mapper();
        $rest = $mapper->getPaginas();
        $jsonData = Zend_Json::encode(
                        array('items' => $rest));
        $this->getResponse()->setBody($jsonData);        
    }
   
}
