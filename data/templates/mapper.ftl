<?php
/**
 * Class for Mapper SQL.
 *
 * @category   Model
 * @package    ${namespacemapper}
 * @copyright  Copyright (c) 2013 CookieShop.
 * @license    http://www.gnu.org/licenses/gpl-3.0-standalone.html GNU GPL v.3.0
 * @author     Eduardo Ortiz <eduardooa1980@gmail.com>
 */
class ${namespacemapper} extends ZendX_Model_Mapper_Abstract{
    
    /**
     * Set adapter and DB alias
     * 
     * DB alias setter maintain null, otherwise use it as follows:
     * $this->setAliasadpter(array('db' => 'db1'));
     * 
     * __construct()
     * 
     */
    public function __construct() {
        $this->setNsadapter('${namespaceadapter}');
        $this->setAliasadpter(${alias});
    }
    
    /**
     * 
     * @param type $resultSet
     * @return type
     */
    private function _getRead($resultSet) {
        $entries = array();
        foreach ($resultSet as $row) {
                $entries[] = $this->_parseData($row);
        }
        return $entries;
    }
    
    /**
     * 
     * @param type $row
     * @return \${namespacentity}
     */
    private function _parseData($row){
        $entry = new ${namespacentity}();
        <#list entitiescamel.rows as row> 
        $entry->set${row.getNamecamel()}($row->${row.getNamerow()});
        </#list> 
        return $entry;
    }
    
    /**
     * 
     * @param type $entries
     * @return array
     */
    private function _getDataToShow($entries) {
        $entrie = array();
        foreach ($entries as $item) {
                array_push($entrie,$this->_getDataToArray($item));
        }
        return $entrie;
    }
    
    /**
     * 
     * @param type $item
     * @return type
     */
    private function _getDataToArray($item){
        $return = array();
        if($item!==null){
            $return = array(
            <#list entities as entity>    
                '${entity}'=>$item->${entity},
            </#list>    
            );
        }
        return $return;
    }
    
    /**
     * 
     * @param ${namespacentity} $${nametable}
     * @return type
     */
    private function _getData(${namespacentity} $${nametable}) {
        $data = array(
            <#list entitiescamel.rows as row> 
            '${row.getNamerow()}' => $${nametable}->get${row.getNamecamel()}(),
            </#list>         
        );
        return $data;

    }
    
    /**
     * getMetadata()
     * 
     * Get Metadata SQL
     * 
     * @return type
     */
    public function getMetadata() {
        return $this->getDbTable()->info();
    }
    
    /**
     * 
     * @param type $page
     * @return type
     */    
    public function read($page = 1) {
        $resultSet = $this->fetchAll($page);
        $entries = $this->_getRead($resultSet);
        return $this->_getDataToShow($entries);
    }
    
    /**
     * how to use:
     * $params = array(
     *     array('condition'=>'id_role= ?','value'=>$data['id_role']),
     *     array('condition'=>'id_resource= ?','value'=>$data['id_resource'])
     * );
     * this array, keeps the duplication of rows
     * 
     * @param ${namespacentity} $${nametable}
     */
    public function write(${namespacentity} $${nametable}) {
        $data = $this->_getData($${nametable});
        $params = array();
        $this->insertOrUpdate($data, $params);
    } 
    
    /**
     * 
     * @param ${namespacentity} $settings
     * @param type $keysubform
     * @return type
     */
    public function findid(${namespacentity} $${nametable}, $keysubform) {
        $data = array('id' => $${nametable}->getId());
        $items = null;
        $result = $this->findOneById($data);
        if ($result !== null) {
            $items = array();
            foreach ($keysubform as $enty) {
                foreach ($result->toArray() as $key => $item) {
                        $newkey = preg_replace('/_/', '', $key);
                        $items[$enty . '-' . $newkey] = $item;             
                }
            }
        }
        return $items;
    } 
    
    /**
     * 
     * @param ${namespacentity} $settings
     */
    public function deleteRoles(${namespacentity} $${nametable}){
        $select = $this->getDbTable()
                ->select()
                ->where('id= ?', $${nametable}->getId());        
        $result = $this->getDbTable()->fetchRow($select);
        //if($result['id_role']!=='2'&&$result['id_role']!=='1'){
            $this->deleteById($${nametable}->getId());           
        //}
    } 
    
    /**
     * how to use:
     * $params=array(
     *    array(
     *        'val1'=>$settings->getId(),
     *        'val2'=>0,'oper'=>'>',
     *        'condition'=>'id = ?',
     *        'value'=>$settings->getId()),
     *   );
     * @param ${namespacentity} $settings
     * @return type
     */
    public function filtros(${namespacentity} $${nametable}){
        
        $params=array(
            
        );
        $resultSet = $this->filterByCondition($params);
        $entries = $this->_getRead($resultSet);
        return $this->_getDataToShow($entries);        
    }
    
   /**
     * 
     * @param type $limit
     * @return type
     */
    public function getPaginas($limit = 25) {
        return $this->getPaginator('${table}', $limit);

    } 
    
}
