<?php
/**
 * Class for contruct dojo form.
 *
 * @category   Model
 * @package    ${namespacemapper}
 * @copyright  Copyright (c) 2013 CookieShop.
 * @license    http://www.gnu.org/licenses/gpl-3.0-standalone.html GNU GPL v.3.0
 * @author     Eduardo Ortiz <eduardooa1980@gmail.com>
 */
class ${namespacemapper} extends Zend_Dojo_Form {
    /**
     * init()
     */
    public function init() {
        $${idkey} = new Zend_Form_Element_Hidden('${idkey}');
        $this->setAttribs(array(
            'name' => '${nameform}',
            'style' => 'width:400px;height:350px'));
        $datosForm = new Zend_Dojo_Form_SubForm();
        $datosForm->addElement($${idkey})
<#list entitiescamel.rows as row>
            ->addElement(
                    'ValidationTextBox', 
                    '${row.getEntities()}', array(
                        'value' => '', 
                        'label' => '${row.getLabel()}',${row.getRegexp()} 
                        'trim' => true,${row.getRequerid()}))
 </#list> 
	    ->addElement(
		    'Button',
		    'guardaJson', 
		    array(
			'required' => false,
			'ignore' => true,
			'label' => 'Guardar', ))
            ->addElement(
                    'Button',
                    'cancelarJson',
                    array(
                        'required' => false,
                        'ignore' => true,
                        'label' => 'Cancelar',
                        'onClick' => 
                        'cancelar("${nameform}",'.
                        '"${namedialog}")'));
        
        $this->addSubForm($datosForm, '${namesubform}');
<#list entitiescamel.rows as row>        
        $this->${namesubform}->${row.getEntities()}->addDecorators(
                array('Foobar' => 'HtmlTag'), 
                array('tag' => 'div'));
 </#list> 
        $this->${namesubform}->guardaJson->addDecorators(
                array('Foobar' => 'HtmlTag'), 
                array('tag' => 'div'));
        $this->${namesubform}->cancelarJson->addDecorators(
                array('Foobar' => 'HtmlTag'), 
                array('tag' => 'div'));
    }
}
