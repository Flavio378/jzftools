<?php

/**
 * Class for Setting adapter.
 *
 * @category   Model
 * @package    ZendX_Dojo_Layer_${namespacemapper}
 * @copyright  Copyright (c) 2013 CookieShop.
 * @license    http://www.gnu.org/licenses/gpl-3.0-standalone.html GNU GPL v.3.0
 * @author     Eduardo Ortiz <eduardooa1980@gmail.com>
 */
class ZendX_Dojo_Layer_${namespacemapper}{
    /**
     * 
     * @param type $id
     * @return type
     * 
     */
    public function getMetadata($id){
        return array(
        array(
        'nombreVar'=>'write_'.$id,    
        'function'=>'sendWrite',
        'params'=>array(
            'idForm'=>'"${nameform}new"',
            'btnSave'=>'"${nameform}New-guardaJson"',
            'idDialog'=>'"${idialognew}"',
            'urixhr'=>'"${resource}/write"',
            'idgrid'=>'"grid_'.$id.'"',
            'iddivgrid'=>'"divgrid_'.$id.'"',
            'urigrid'=>'"${resource}/index"',
            'layout'=>'"layout${namespacelayout}"'),
        ),
        array(
        'nombreVar'=>'write_edit_'.$id,    
        'function'=>'sendWrite',
        'params'=>array(
            'idForm'=>'"${nameform}edit"',
            'btnSave'=>'"${nameform}Edit-guardaJson"',
            'idDialog'=>'"${idialogedit}"',
            'urixhr'=>'"${resource}/write"',
            'idgrid'=>'"grid_'.$id.'"',
            'iddivgrid'=>'"divgrid_'.$id.'"',
            'urigrid'=>'"${resource}/index"',
            'layout'=>'"layout${namespacelayout}"'),
        ),       
        array( 
        'nombreVar'=>'filtrar_'.$id,    
        'function'=>'sendPage',
        'params'=>array(
            'idForm'=>'"${nameform}filava"',
            'btnSave'=>'"${nameform}Filava-guardaJson"',
            'idDialog'=>'"${idialogfilava}"',
            'urixhr'=>'"${resource}/filtros"',
            'idgrid'=>'"grid_'.$id.'"',
            'iddivgrid'=>'"divgrid_'.$id.'"',
            'urigrid'=>'"${resource}/index"',
            'layout'=>'"layout${namespacelayout}"'),
        ),  
        );
    }

}
