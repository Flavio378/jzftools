<?php

/**
 * Class for ${nametype}.
 *
 * @category   Model
 * @package    ZendX_Dojo_${nametype}_Metadata
 * @copyright  Copyright (c) 2013 CookieShop.
 * @license    http://www.gnu.org/licenses/gpl-3.0-standalone.html GNU GPL v.3.0
 * @author     Eduardo Ortiz <eduardooa1980@gmail.com>
 */
class ZendX_Dojo_${nametype}_Metadata{
    /**
     * 
     * @return type
     */
    public function getClases(){
        $path = realpath(APPLICATION_PATH . '/../library/ZendX/Dojo');
        $arrayreg = require $path.'/${nametype}/config.php';
        return $arrayreg;
    }
    /**
     * 
     * @param type $resource   
     * @return type
     */
    public function getMetadatas($resource){
       $para = array();
        $clases = $this->getClases();
        foreach ($clases as $key => $value) {
            if($this->isClase($resource,$value['resource'])){ 
                $para = $value['class'];                
            }
        }
        return $para;
    }
    /**
     * 
     * @param type $cadena
     * @param type $buscar
     * @return boolean
     */
    public function isClase($cadena,$buscar){
        $isclase = false;
         $str = explode('/',$cadena);
         $resultado = strcmp($str[0], $buscar);
         if($resultado===0){
             $isclase = true;
         }
        return $isclase;
    }  
}
