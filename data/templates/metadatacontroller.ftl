<?php

/**
 * Class for ${nametype}.
 *
 * @category   Model
 * @package    ZendX_Dojo_${nametype}_${module}_Metadata
 * @copyright  Copyright (c) 2013 CookieShop.
 * @license    http://www.gnu.org/licenses/gpl-3.0-standalone.html GNU GPL v.3.0
 * @author     Eduardo Ortiz <eduardooa1980@gmail.com>
 */
class ZendX_Dojo_${nametype}_${module}_Metadata{
    /**
     * 
     * @return type
     */
    public function getClases(){
        $path = realpath(APPLICATION_PATH . '/../library/ZendX/Dojo');
        $arrayreg = require $path.'/${nametype}/${module}/config.php';
        return $arrayreg;
    }
    /**
     * 
     * @param type $resource
     * @param type $id
     * @return type
     */
    public function getMetadatas($resource,$id=null){
       $para = array();
        $clases = $this->getClases();
        foreach ($clases as $key => $value) {
            if($this->isClase($resource,$value['resource'])){ 
                $obj = new $value['class'];
                if(is_null($id)){
                   $para = $obj->getMetadata(); 
                }else{
                  $para = $obj->getMetadata($id);  
                }
                
                
            }
            
        }
        return $para;
    }
    /**
     * 
     * @param type $cadena
     * @param type $buscar
     * @return boolean
     */
    public function isClase($cadena,$buscar){
        $isclase = false;
         $resultado = strcmp($cadena, $buscar);
         if($resultado===0){
             $isclase = true;
         }
        return $isclase;
    }
}
