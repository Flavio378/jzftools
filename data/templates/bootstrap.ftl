<?php

/**
 * Class for ${module}.
 *
 * @category   Model
 * @package    ${module}_Bootstrap
 * @copyright  Copyright (c) 2013 CookieShop.
 * @license    http://www.gnu.org/licenses/gpl-3.0-standalone.html GNU GPL v.3.0
 * @author     Eduardo Ortiz <eduardooa1980@gmail.com>
 */
class ${module}_Bootstrap extends Zend_Application_Module_Bootstrap{

}
