
package zendroo;

import javax.swing.JFrame;
import zendroo.formularios.PasswordRoot;

/**
 *
 * @author DGomez
 */
public class ZendRoo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        PasswordRoot p = new PasswordRoot();
        p.setTitle("JZFTools");
        p.setResizable(false);
        p.setLocationRelativeTo(null);
        p.setVisible(true);

    }

}
