/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zendroo.create;


import com.manipularfile.DirectorioJar;
import entities.Templateenhancegrid;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zendroo.utilities.ExpresionesRegulares;

/**
 *
 * @author DGomez
 */
public class CreateEnhancedgrid extends zendroo.file.Files {

    public boolean createFile(String namespace, String tableName,
            ArrayList<String> table) throws IOException, TemplateException {
        DirectorioJar pathreal = new DirectorioJar();
        String separator = String.valueOf(java.io.File.separatorChar);
        tableName = ExpresionesRegulares.replacer(tableName, "_", "");
        
        
        /* Create and adjust the configuration */
        Configuration cfg = new Configuration();
        cfg.setDirectoryForTemplateLoading(new File(
                pathreal.getPath()+"/data/templates"));
        cfg.setObjectWrapper(new DefaultObjectWrapper());
        cfg.setDefaultEncoding("UTF-8");
        cfg.setIncompatibleImprovements(new Version(2, 3, 20));

        /* Create a data-model */
        Map root = new HashMap();
         
        String namespacemapper = "ZendX_Dojo_EnhancedGrid_"+ namespace + 
                "_" + this.getNameFunction(tableName);
        root.put("namespacemapper", namespacemapper);       

        List<Object> rows = new ArrayList<Object>();
        
        for (int i = 0; i < table.size(); i++) {
            String col = ExpresionesRegulares.replacer(table.get(i), "_", "");
            rows.add(
                    new Templateenhancegrid(col,this.getNameFunction(col)));
        }        
        
        
        Map<String, Object> entitiescamel = new HashMap<String, Object>();
        entitiescamel.put("rows",rows);

        root.put("entitiescamel",entitiescamel);         
        
        /* Get the template */
        Template template = cfg.getTemplate("regenhancedgrid.ftl");  
        String path = this.getPath() + "library"+ separator+"ZendX"+ separator+
                "Dojo"+ separator+"EnhancedGrid"+ separator+namespace + separator;
        String filename = this.getNameFunction(tableName)+".php";

        File f = new File(path);
        f.mkdirs();
        Writer writer = new FileWriter(new File(path+filename));
        template.process(root, writer);                
        
        return true;

    }
}
