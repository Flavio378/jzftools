/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zendroo.create;


import com.manipularfile.DirectorioJar;
import entities.Templatelayout;
import freemarker.template.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zendroo.utilities.ExpresionesRegulares;

/**
 *
 * @author DGomez
 */
public class CreateLayout extends zendroo.file.Files {

    public boolean createFile(String namespace, String tableName,
            ArrayList<String> table) throws IOException, TemplateException {
        DirectorioJar pathreal = new DirectorioJar();
        boolean save = false;
        String separator = String.valueOf(java.io.File.separatorChar);
        tableName = ExpresionesRegulares.replacer(tableName, "_", "");
        
        /* Create and adjust the configuration */
        Configuration cfg = new Configuration();
        cfg.setDirectoryForTemplateLoading(new File(
                pathreal.getPath()+"/data/templates"));
        cfg.setObjectWrapper(new DefaultObjectWrapper());
        cfg.setDefaultEncoding("UTF-8");
        cfg.setIncompatibleImprovements(new Version(2, 3, 20));
        String layoutname =namespace+tableName.toLowerCase();
        
        Map root = new HashMap();
        root.put("controller", tableName.toLowerCase());
        root.put("layoutname", layoutname);
        
        List<Object> rows = new ArrayList<Object>();
        
        for (int i = 0; i < table.size(); i++) {
            String col = ExpresionesRegulares.replacer(table.get(i), "_", "");
            rows.add(
                    new Templatelayout(col,this.getNameFunction(col),layoutname,col));
        }  
        Map<String, Object> entitiescamel = new HashMap<String, Object>();
        entitiescamel.put("rows",rows);

        root.put("entitiescamel",entitiescamel);         
        
        /* Get the template */
        Template template = cfg.getTemplate("layout.ftl");  
        String path = this.getPath() + "public"+ separator+"js"+ separator+
                "layer"+ separator+"onclick"+ separator;
        String filename = namespace.toLowerCase()+".js";

        File f = new File(path);
        f.mkdirs();
        Writer writer = new FileWriter(new File(path+filename),true);
        template.process(root, writer);        
        save =true;
        return save;
    }
}
