/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zendroo.create;




import com.manipularfile.DirectorioJar;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import zendroo.utilities.ExpresionesRegulares;

/**
 *
 * @author DGomez
 */
public class CreateController extends zendroo.file.Files {

    public boolean createFile(String namespace, String tableName) 
            throws IOException, TemplateException {
        DirectorioJar pathreal = new DirectorioJar();
        boolean save = false;
        String separator = String.valueOf(java.io.File.separatorChar);

        tableName = ExpresionesRegulares.replacer(tableName, "_", "");
        
        /* Create and adjust the configuration */
        Configuration cfg = new Configuration();
        cfg.setDirectoryForTemplateLoading(new File(pathreal.getPath()+"/data/templates"));
        cfg.setObjectWrapper(new DefaultObjectWrapper());
        cfg.setDefaultEncoding("UTF-8");
        cfg.setIncompatibleImprovements(new Version(2, 3, 20));

        /* Create a data-model */
        Map root = new HashMap();      
        
        root.put("module", namespace);
        root.put("controller", this.getNameFunction(tableName));
        root.put("nameform", namespace.toLowerCase()+tableName.toLowerCase());
        
        /* Get the template */
        Template template = cfg.getTemplate("controller.ftl");  
             String path = this.getPath() + "application"+separator+"modules"+
                separator+namespace.toLowerCase() + separator + "controllers"+separator;
        String filename = this.getNameFunction(tableName)+"Controller.php";
        File f = new File(path);
        f.mkdirs();
        Writer writer = new FileWriter(new File(path+filename));
        template.process(root, writer);        
        
        save = true;       
        return save;


    }
}

