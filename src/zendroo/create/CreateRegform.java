/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zendroo.create;



import com.manipularfile.DirectorioJar;
import entities.Templateregform;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zendroo.utilities.ExpresionesRegulares;

/**
 *
 * @author DGomez
 */
public class CreateRegform extends zendroo.file.Files {

    public boolean createFile(String namespace, String tableName,
            ArrayList<String> table,
            ArrayList<String> required,
            ArrayList<String> regexp) throws IOException, TemplateException {
        DirectorioJar pathreal = new DirectorioJar();
        boolean save = false;
        String separator = String.valueOf(java.io.File.separatorChar);

        tableName = ExpresionesRegulares.replacer(tableName, "_", "");
        
        /* Create and adjust the configuration */
        Configuration cfg = new Configuration();
        cfg.setDirectoryForTemplateLoading(new File(
                pathreal.getPath()+"/data/templates"));
        cfg.setObjectWrapper(new DefaultObjectWrapper());
        cfg.setDefaultEncoding("UTF-8");
        cfg.setIncompatibleImprovements(new Version(2, 3, 20));

        /* Create a data-model */
        Map root = new HashMap();
       
       

        String namespacemapper = "ZendX_Dojo_Form_"+ namespace + 
                "_" + this.getNameFunction(tableName);
        
        root.put("namespacemapper", namespacemapper);

        

        List<Object> rows = new ArrayList<Object>();        

        String namespaceform = namespace + "_Form_" + this.getNameFunction(tableName) + "new";
        String iddialog =namespace.toLowerCase()+this.getNameFunction("new")+
                "Dialog"+ this.getNameFunction(tableName);
        rows.add(new Templateregform(iddialog,namespaceform,"Nuevo Registro"));
        
        
        namespaceform = namespace + "_Form_" + this.getNameFunction(tableName) + "edit";
        iddialog =namespace.toLowerCase()+this.getNameFunction("edit")+
                "Dialog"+ this.getNameFunction(tableName);
        rows.add(new Templateregform(iddialog,namespaceform,"Editar Registro"));
        
        
        namespaceform = namespace + "_Form_" + this.getNameFunction(tableName) + "filava";
        iddialog =namespace.toLowerCase()+this.getNameFunction("filava")+
                "Dialog"+ this.getNameFunction(tableName);        
        rows.add(new Templateregform(iddialog,namespaceform,"Filtrar Registro"));
        
        
        Map<String, Object> entitiescamel = new HashMap<String, Object>();
        entitiescamel.put("rows",rows);

        root.put("entitiescamel",entitiescamel);         
        
        /* Get the template */
        Template template = cfg.getTemplate("regform.ftl");  
       String path = this.getPath() + "library"+ separator+"ZendX"+ separator+
                "Dojo"+ separator+"Form"+ separator+namespace + separator;
        String filename = this.getNameFunction(tableName)+".php";
        File f = new File(path);
        f.mkdirs();
        Writer writer = new FileWriter(new File(path+filename));
        template.process(root, writer);        
        
        save = true;       
        return save;


    }
}

