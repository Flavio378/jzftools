
package zendroo.create;

import com.manipularfile.DirectorioJar;
import entities.Templateentities;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.Version;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zendroo.utilities.ExpresionesRegulares;
import zendroo.utilities.Mensajes;

/**
 *
 * @author DGomez
 */
public class CreateParser extends zendroo.file.Files {

    public CreateParser() {
    }

    public boolean createFile(String namespace, String tableName, ArrayList<String> types, ArrayList<String> table) {
        /*Generar encabezado del archivo*/
        DirectorioJar pathreal = new DirectorioJar();
        boolean save = false;
        String separator = String.valueOf(java.io.File.separatorChar);
        try {
            tableName = this.getNameFunction(ExpresionesRegulares.replacer(tableName, "_", ""));
            
         /* Create and adjust the configuration */
            Configuration cfg = new Configuration();
            cfg.setDirectoryForTemplateLoading(new File(
                    pathreal.getPath()+"/data/templates"));
            cfg.setObjectWrapper(new DefaultObjectWrapper());
            cfg.setDefaultEncoding("UTF-8");
            cfg.setIncompatibleImprovements(new Version(2, 3, 20));
            
            /* Create a data-model */
            Map root = new HashMap();
            root.put("namespacemapper", namespace + "_Model_" + tableName); 

            List<Object> rows = new ArrayList<Object>();
        
            for (int i = 0; i < table.size(); i++) {
                String col = ExpresionesRegulares.replacer(table.get(i), "_", "");
                rows.add(new Templateentities(col,this.getNameFunction(col),types.get(i)));
            }
            Map<String, Object> entitiescamel = new HashMap<String, Object>();
            entitiescamel.put("rows",rows);           

            root.put("entitiescamel",entitiescamel); 
            
            /* Get the template */
            Template template = cfg.getTemplate("parser.ftl");  
        String path = this.getPath() + "application"+separator+"modules"+
                separator+namespace.toLowerCase() + separator + "models"+separator;
            String filename = this.getNameFunction(tableName)+".php";
            
            File f = new File(path);
            f.mkdirs();
            Writer writer = new FileWriter(new File(path+filename));
            template.process(root, writer);
            save= true; 
        } catch (Exception ex) {
            Mensajes.error("" + ex.getMessage());
        }
        return save;
    }
}
