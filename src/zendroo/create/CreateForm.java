/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zendroo.create;

import com.manipularfile.DirectorioJar;
import entities.Templateform;
import freemarker.template.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zendroo.utilities.ExpresionesRegulares;

/**
 *
 * @author DGomez
 */
public class CreateForm extends zendroo.file.Files {

    public boolean createFile(String namespace, String tableName,
            ArrayList<String> table,
            ArrayList<String> required,
            ArrayList<String> regexp,
            String nsform) throws IOException, TemplateException {
        DirectorioJar pathreal = new DirectorioJar();
        boolean save = false;
        String separator = String.valueOf(java.io.File.separatorChar);

        tableName = ExpresionesRegulares.replacer(tableName, "_", "");
        
        /* Create and adjust the configuration */
        Configuration cfg = new Configuration();
        cfg.setDirectoryForTemplateLoading(new File(
                pathreal.getPath()+"/data/templates"));
        cfg.setObjectWrapper(new DefaultObjectWrapper());
        cfg.setDefaultEncoding("UTF-8");
        cfg.setIncompatibleImprovements(new Version(2, 3, 20));

        /* Create a data-model */
        Map root = new HashMap();
        String nf =namespace.toLowerCase()+tableName.toLowerCase()+
                this.getNameFunction(nsform);        
        String nd =namespace.toLowerCase()+this.getNameFunction(nsform)+
                "Dialog"+ this.getNameFunction(tableName);        
        
        root.put("namespacemapper", namespace + "_Form_" + this.getNameFunction(tableName) + nsform);
        root.put("nametable", tableName.toLowerCase());        
        root.put("idkey", "id");
        root.put("nameform", namespace.toLowerCase()+
                tableName.toLowerCase()+nsform);   
        root.put("namesubform", nf);
        root.put("namedialog",nd);
        

        List<Object> rows = new ArrayList<Object>();
        
        for (int i = 0; i < table.size(); i++) {
            String col = ExpresionesRegulares.replacer(table.get(i), "_", "");
            rows.add(
                    new Templateform(col,this.getNameFunction(col),
                    regexp.get(i),required.get(i)));
        }        
        
        
        Map<String, Object> entitiescamel = new HashMap<String, Object>();
        entitiescamel.put("rows",rows);

        root.put("entitiescamel",entitiescamel);         
        
        /* Get the template */
        Template template = cfg.getTemplate("form.ftl");  
        String path = this.getPath() + "application"+separator+"modules"+
                separator+namespace.toLowerCase() + separator + "forms"+separator;
        String filename = this.getNameFunction(tableName)+nsform+".php";

        File f = new File(path);
        f.mkdirs();
        Writer writer = new FileWriter(new File(path+filename));
        template.process(root, writer);       
        
 
        save = true;       
        return save;
    }

    private static class crtRegform {

        public crtRegform() {
        }
    }
}
