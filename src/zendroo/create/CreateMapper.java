/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zendroo.create;

import com.manipularfile.DirectorioJar;
import java.util.ArrayList;
import zendroo.utilities.ExpresionesRegulares;
import zendroo.utilities.Mensajes;
import freemarker.template.*;
import java.util.*;
import java.util.List;
import java.io.*;
import entities.Templatemapper;
/**
 *
 * @author DGomez
 */
public class CreateMapper extends zendroo.file.Files {
    
    public boolean createFile(String namespace, String tableName, 
            ArrayList<String> table,String alias) {
        /*
         * Generar encabezado del archivo
         */
        DirectorioJar pathreal = new DirectorioJar();
        String separator = String.valueOf(java.io.File.separatorChar);
        boolean save = false;
        try {
            String temp = tableName;
            tableName = this.getNameFunction(ExpresionesRegulares.replacer(tableName, "_", ""));
            
            /* Create and adjust the configuration */
            Configuration cfg = new Configuration();
            cfg.setDirectoryForTemplateLoading(new File(
                    pathreal.getPath()+"/data/templates"));
            cfg.setObjectWrapper(new DefaultObjectWrapper());
            cfg.setDefaultEncoding("UTF-8");
            cfg.setIncompatibleImprovements(new Version(2, 3, 20));
            alias = (alias.isEmpty())?"null":"array('db'=>'"+alias+"')";
            /* Create a data-model */
            Map root = new HashMap();
            root.put("namespacemapper", namespace + "_Model_" + tableName + "Mapper");
            root.put("namespaceadapter", namespace + "_Model_DbTable_"+ tableName);
            root.put("namespacentity", namespace + "_Model_" + tableName);
            root.put("nametable", tableName.toLowerCase());
            root.put("table",temp);
            root.put("alias",alias);
            
            
            List<String> entities = new ArrayList<String>();
            List<Object> rows = new ArrayList<Object>();
        
            for (int i = 0; i < table.size(); i++) {
                String col = ExpresionesRegulares.replacer(table.get(i), "_", "");
                entities.add(col);
                rows.add(new Templatemapper(table.get(i),this.getNameFunction(col)));
            }
            Map<String, Object> entitiescamel = new HashMap<String, Object>();
            entitiescamel.put("rows",rows);
            
            root.put("entities", entities);
            root.put("entitiescamel",entitiescamel); 
            
            /* Get the template */
            Template template = cfg.getTemplate("mapper.ftl");  
             String path = this.getPath() + "application"+separator+"modules"+
                separator+namespace.toLowerCase() + separator + "models"+separator;
            String filename = this.getNameFunction(tableName)+"Mapper"+".php";
            
            File f = new File(path);
            f.mkdirs();
            Writer writer = new FileWriter(new File(path+filename));
            template.process(root, writer);
            save = true;
         
        } catch (Exception ex) {
            Mensajes.error("" + ex.getMessage());
        }
        return save;
    }

   
}
