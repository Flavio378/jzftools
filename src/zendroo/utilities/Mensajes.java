/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zendroo.utilities;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Server
 */
public class Mensajes {

    public static void error(String mensaje) {
        JOptionPane.showMessageDialog(null,
                mensaje,
                "Error",
                JOptionPane.ERROR_MESSAGE);
    }

    public static int confirmar(String mensaje) {
        return JOptionPane.showConfirmDialog(null,
                mensaje,
                "Confirmar acción",
                JOptionPane.YES_NO_OPTION);
    }

    public static void informar(String mensaje) {
        JOptionPane.showMessageDialog(null,
                mensaje,
                "Información",
                JOptionPane.INFORMATION_MESSAGE);
    }

    public static String getPathDirectory() {
        try {
            JFileChooser fc = new JFileChooser();
            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int returnVal = fc.showOpenDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {

                File file = fc.getSelectedFile();
                return file.getAbsolutePath()+java.io.File.separatorChar;

            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }
}
