/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zendroo.utilities;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *
 * @author DGomez
 */
public class ExpresionesRegulares {
    public boolean find(String texto, String exp) {
        Pattern pat = Pattern.compile(exp);
        Matcher mat = pat.matcher(texto);
        if (mat.find()) {
            return true;
        } else {
            return false;
        }
    }
    public static String replacer(String texto, String find, String replace) {
        return texto.replaceAll(find, replace); 
    }
}
