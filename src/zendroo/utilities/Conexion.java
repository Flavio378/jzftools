
package zendroo.utilities;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Conexion {

    private Connection conexion;
    private UsuariosBD usr;

    public Conexion(UsuariosBD usr) {
        this.usr = usr;
    }

    //private final String pass = "";
    /**
     * Método utilizado para recuperar el valor del atributo conexion
     *
     * @return conexion contiene el estado de la conexión
     */
    public Connection getConexion() {
        return conexion;
    }

    /**
     * Método utilizado para establecer la conexión con la base de datos
     *
     * @return estado regresa el estado de la conexión, true si se estableció la
     * conexión, falso en caso contrario
     */
    public boolean crearConexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection("jdbc:mysql://" + this.usr.getHost() + ":" + this.usr.getPuerto() + "/" + this.usr.getBd(), this.usr.getUsuario(), this.usr.getPass());
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No se encuentra la Base de Datos, "
                    + "\nAsegúrese que el servicio de MySql esté activo y que la el usuario y contraseña coincidan",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "No se encuentra la Base de Datos, "
                    + "\nAsegúrese que el servicio de MySql esté activo y que la el usuario y contraseña coincidan",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }

        return true;
    }

    /**
     *
     * Método utilizado para realizar las instrucciones: INSERT, DELETE y UPDATE
     *
     * @param sql Cadena que contiene la instrucción SQL a ejecutar
     * @return estado regresa el estado de la ejecución, true(éxito) o
     * false(error)
     */
    public boolean ejecutarSQL(String sql) {
        try {
            Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(sql);
//            conexion.close();
            return true;
        } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, "Error al guardar el registro, "
                    + "verifique la información",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    /**
     *
     * Método utilizado para realizar la instrucción SELECT
     *
     * @param sql Cadena que contiene la instrucción SQL a ejecutar
     * @return resultado regresa los registros generados por la consulta
     */
    public ResultSet ejecutarSQLSelect(String sql) {
        ResultSet resultado;
        try {
            Statement sentencia = conexion.createStatement();
            resultado = sentencia.executeQuery(sql);
//            conexion.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }

        return resultado;
    }

    public void cerrarConexion() {
        try {
            conexion.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int generaID(String tabla, String nombreCampoID) {
        int id = 0;
        try {
            ResultSet rs = this.ejecutarSQLSelect("select " + nombreCampoID + " FROM " + tabla);
            if (rs.last()) {
                id = rs.getInt(1);
                id++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id;
    }

    public ResultSet ListTables(String nombre) {
        try {
            this.usr.setBd(nombre);
            this.crearConexion();

            // Enable logging
            // DriverManager.setLogStream(System.err);

            Connection c;

            c = this.getConexion();

            DatabaseMetaData md = c.getMetaData();
            ResultSet rs = md.getTables(null, null, "%", null);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
