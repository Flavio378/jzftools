/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zendroo.utilities;

/**
 * 
 * @author davichos
 */
public class Redondear {

    public static double round(double val, int places) {
        double factor = Math.pow(10, places);
        // Shift the decimal the correct number of places
        // to the right.
        val = val * factor;
        // Round to the nearest integer.
        double tmp = Math.round(val);
        // Shift the decimal the correct number of places
        // back to the left.
        return tmp / factor;
    }
}
