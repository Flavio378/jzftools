/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package zendroo.utilities;

/**
 *
 * @author DGomez
 */
public class UsuariosBD {

    private String host;
    private String puerto;
    private String bd;
    private String usuario;
    private String pass;

    public UsuariosBD() {
        this.bd = "mysql";
    }

    public UsuariosBD(String host, String puerto, String bd, String usuario, String pass) {
        this.host = host;
        this.puerto = puerto;
        this.bd = bd;
        this.usuario = usuario;
        this.pass = pass;
    }

    public String getBd() {
        return bd;
    }

    public void setBd(String bd) {
        this.bd = bd;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPuerto() {
        return puerto;
    }

    public void setPuerto(String puerto) {
        this.puerto = puerto;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

}
