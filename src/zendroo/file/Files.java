/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package zendroo.file;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import zendroo.utilities.Mensajes;

/**
 *
 * @author DGomez
 */
public class Files {

    private String fileContents;
    private String fileName;
    private String path;

    public Files() {
    }

    public Files(String fileName, String path) {
        this.fileName = fileName;
        this.path = path;
    }

    public Files(String fileContents, String fileName, String path) {
        this.fileContents = fileContents;
        this.fileName = fileName;
        this.path = path;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPath() {
        File f = new File(path);
        f.mkdirs();
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileContens() {
        return fileContents;
    }

    public void setFileContents(String file) {
        this.fileContents = file;
    }

    public boolean saveString(String fileContents, String fileName, String path) {
        this.setFileName(fileName);
        this.setPath(path);
        this.setFileContents(fileContents);
        return this.saveString();
    }

    public boolean saveString() {
        boolean rturn = false;
        try {
            File f = new File(this.getPath() + this.getFileName());
            if (f.createNewFile() && f.canWrite()) {
                PrintWriter pw = new PrintWriter(f);
                pw.write(this.getFileContens());
                pw.close();
                rturn = true;
            }

        } catch (IOException ex) {
            Logger.getLogger(Files.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rturn;
    }
    
    public String getNameFunction(String name) {
        return name.substring(0, 1).toUpperCase() + name.substring(1, name.length());
    }

}
