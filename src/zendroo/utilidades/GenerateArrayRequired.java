/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package zendroo.utilidades;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import zendroo.utilities.ExpresionesRegulares;

/**
 *
 * @author DGomez
 */
public class GenerateArrayRequired {

    public ArrayList<String> getArray(ResultSet rs) {
        ArrayList<String> data = new ArrayList<String>();
        ExpresionesRegulares e = new ExpresionesRegulares();
        try {
            while (rs.next()) {
                String texto = rs.getString(3).toLowerCase();
                if (texto.equals("yes")) {
                    data.add("'required' => false,");
                } else {
                    if (texto.equals("no")){
                        data.add("'required' => true,");
                    }
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(GeneraArrayTipeo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }

}
