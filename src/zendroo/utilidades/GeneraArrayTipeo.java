/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zendroo.utilidades;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import zendroo.utilities.ExpresionesRegulares;
import zendroo.utilities.Mensajes;

/**
 *
 * @author DGomez
 */
public class GeneraArrayTipeo {

    public ArrayList<String> getArray(ResultSet rs) {
        ArrayList<String> data = new ArrayList<String>();
        ExpresionesRegulares e = new ExpresionesRegulares();
        try {
            while (rs.next()) {
                String texto = rs.getString(2).toLowerCase();
                if (e.find(texto, "int")
                        || e.find(texto, "numeric")) {
                    data.add("int");
                } else {
                    if (e.find(texto, "float")
                            || e.find(texto, "decimal")) {
                        data.add("float");
                    } else {
                        if (e.find(texto, "double")) {
                            data.add("double");
                        } else {
                            data.add("string");
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(GeneraArrayTipeo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }
    
    public ArrayList<String> getArrayRegexp(ResultSet rs) {
        ArrayList<String> data = new ArrayList<String>();
        ExpresionesRegulares e = new ExpresionesRegulares();
        try {
            while (rs.next()) {
                String texto = rs.getString(2).toLowerCase();
                if (e.find(texto, "int")
                        || e.find(texto, "numeric")) {
                    data.add("'regExp' => '^[0-9]+',");
                } else {
                    if (e.find(texto, "float")) {
                        data.add("'regExp' => '^[0-9]*[.]?[0-9]+$',");
                    } else {
                        if (e.find(texto, "double")) {
                            data.add("'regExp' => '^[0-9]*[.]?[0-9]+$',");
                        } else {
                            data.add("");
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(GeneraArrayTipeo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }
    
}
