/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package zendroo.utilidades;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import zendroo.utilities.ExpresionesRegulares;

/**
 *
 * @author DGomez
 */
public class GeneraArrayColumns {

    public ArrayList<String> getArray(ResultSet rs) {
        ArrayList<String> data = new ArrayList<String>();
        ExpresionesRegulares e = new ExpresionesRegulares();
        try {
            while (rs.next()) {
                String texto = rs.getString(1).toLowerCase();
                
                    data.add(texto);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(GeneraArrayTipeo.class.getName()).log(Level.SEVERE, null, ex);
        }catch (Exception ex) {
            Logger.getLogger(GeneraArrayTipeo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }

}
