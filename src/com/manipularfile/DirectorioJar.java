/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manipularfile;

import java.io.File;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dev
 */
public class DirectorioJar {
    public boolean isAbsolute = true;
    public String getPathJar() throws URISyntaxException{
        CodeSource codeSource = DirectorioJar.class.getProtectionDomain().
                getCodeSource();
        File jarFile = new File(codeSource.getLocation().toURI().getPath());
        String jarDir = jarFile.getParentFile().getPath();  
        return jarDir;
    }

    public String getPathAbsolute(){
        File dir = new File("");
        return dir.getAbsolutePath();
    }  
    
    public String getPath(){
        String path = null;
        if(this.isAbsolute){
            File dir = new File("");
            path = dir.getAbsolutePath();            
        }else{
            try {
                CodeSource codeSource = DirectorioJar.class.
                        getProtectionDomain().getCodeSource();
                File jarFile = new File(
                        codeSource.getLocation().toURI().getPath());
                String jarDir = jarFile.getParentFile().getPath();            
                path = jarDir;
            } catch (URISyntaxException ex) {
                Logger.getLogger(DirectorioJar.class.getName()).log(
                        Level.SEVERE, null, ex);
            }
        }
        return path;
    }
}
