/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author dev
 */
public class Templateentities {
     private String entities;
     private String namecamel;
     private String typedat; 

    public Templateentities() {
    }

    public Templateentities(String namerow, String namecamel,String typedat) {
       this.entities = namerow;
       this.namecamel = namecamel;
       this.typedat = typedat;
    }

    public String getEntities() {
        return entities;
    }

    public void setEntities(String entities) {
        this.entities = entities;
    }

    public String getNamecamel() {
        return namecamel;
    }

    public void setNamecamel(String namecamel) {
        this.namecamel = namecamel;
    }

    public String getTypedat() {
        return typedat;
    }

    public void setTypedat(String typedat) {
        this.typedat = typedat;
    }
   
}
