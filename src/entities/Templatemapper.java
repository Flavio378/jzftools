/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author dev
 */
public class Templatemapper {
     private String namerow;
     private String namecamel;

    public Templatemapper() {
    }

    public Templatemapper(String namerow, String namecamel) {
       this.namerow = namerow;
       this.namecamel = namecamel;
    }

    public String getNamerow() {
        return namerow;
    }

    public void setNamerow(String namerow) {
        this.namerow = namerow;
    }

    public String getNamecamel() {
        return namecamel;
    }

    public void setNamecamel(String namecamel) {
        this.namecamel = namecamel;
    }    
}
