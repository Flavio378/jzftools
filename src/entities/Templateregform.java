/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author dev
 */
public class Templateregform {
     private String iddialog;
     private String namespaceform;
     private String title; 

    public Templateregform() {
    }

    public Templateregform(String iddialog, String namespaceform,String title) {
       this.iddialog = iddialog;
       this.namespaceform = namespaceform;
       this.title = title;
      
    }

    public String getIddialog() {
        return iddialog;
    }

    public void setIddialog(String iddialog) {
        this.iddialog = iddialog;
    }

    public String getNamespaceform() {
        return namespaceform;
    }

    public void setNamespaceform(String namespaceform) {
        this.namespaceform = namespaceform;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

   
}
