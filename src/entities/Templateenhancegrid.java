/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author dev
 */
public class Templateenhancegrid {
     private String entities;
     private String namecamel;

    public Templateenhancegrid() {
    }

    public Templateenhancegrid(String namerow, String namecamel) {
       this.entities = namerow;
       this.namecamel = namecamel;
    }

    public String getEntities() {
        return entities;
    }

    public void setEntities(String entities) {
        this.entities = entities;
    }

    public String getNamecamel() {
        return namecamel;
    }

    public void setNamecamel(String namecamel) {
        this.namecamel = namecamel;
    }

}
