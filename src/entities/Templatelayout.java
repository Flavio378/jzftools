/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author dev
 */
public class Templatelayout {
     private String field;
     private String name;
     private String controller;
     private String fieldedit; 

    public Templatelayout() {
    }

    public Templatelayout(String field, String name,String controller,String fieldedit) {
       this.field = field;
       this.name = name;
       this.controller=controller;
       this.fieldedit = fieldedit;
     
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    public String getFieldedit() {
        return fieldedit;
    }

    public void setFieldedit(String fieldedit) {
        this.fieldedit = fieldedit;
    }
   
}
