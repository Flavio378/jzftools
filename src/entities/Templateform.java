/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author dev
 */
public class Templateform {
     private String entities;
     private String label;
     private String regexp; 
     private String requerid; 
    public Templateform() {
    }

    public Templateform(String entities, String label,String regexp,String requerid) {
       this.entities = entities;
       this.label = label;
       this.regexp = regexp;
       this.requerid = requerid;       
    }

    public String getEntities() {
        return entities;
    }

    public void setEntities(String entities) {
        this.entities = entities;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRegexp() {
        return regexp;
    }

    public void setRegexp(String regexp) {
        this.regexp = regexp;
    }

    public String getRequerid() {
        return requerid;
    }

    public void setRequerid(String requerid) {
        this.requerid = requerid;
    }

   
}
